package com.aleflet.task.service;

import com.aleflet.task.modal.database.City;
import com.aleflet.task.modal.dto.CityDto;
import com.aleflet.task.modal.exception.RequestValidationException;
import com.aleflet.task.repository.CityRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.Mockito;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CityServiceTests {

    private CityService cityService;

    @BeforeEach
    public void init() {
        CityRepository cityRepository = Mockito.mock(CityRepository.class);
        City firstCity = new City();
        firstCity.setImage("image");
        firstCity.setName("test");
        firstCity.setId(1);

        City secondCity = new City();
        secondCity.setImage("image2");
        secondCity.setName("test2");
        secondCity.setId(2);

        Pageable pageable = Pageable.ofSize(10);
        when(cityRepository.findById(1)).thenReturn(Optional.of(firstCity));
        when(cityRepository.findById(3)).thenReturn(Optional.empty());
        when(cityRepository.findAll(pageable)).thenReturn(new PageImpl<>(List.of(firstCity, secondCity)));
        when(cityRepository.findByNameContainingIgnoreCase("test2", pageable)).thenReturn(new PageImpl<>(List.of(secondCity)));

        cityService = new CityService(cityRepository);
    }

    @Test
    void deleteShouldBeSuccessful() {
        cityService.deleteCity(1);
    }

    @Test
    void retrieveShouldReturnAllValues() {
        CityDto cityDto = cityService.retrieveCities(null, Pageable.ofSize(10));

        assertEquals(2, cityDto.getCities().size());
    }


    @Test
    void searchShouldReturnOneValues() {
        CityDto cityDto = cityService.retrieveCities("test2", Pageable.ofSize(10));

        assertEquals(1, cityDto.getCities().size());
    }


    @Test
    void updateShouldWorkForExisting() throws RequestValidationException {
        cityService.updateCity(1, "testUpdate", "testUpdate");
    }


    @Test
    void updateShouldFailForNoneExisting() {
        assertThrows(RequestValidationException.class, () -> cityService.updateCity(3, "testUpdate", "testUpdate"));
    }

}
