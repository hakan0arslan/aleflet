package com.aleflet.task.util;

import java.util.Objects;

public class StringUtility {

    private static final String EMPTY = "";

    public static boolean isEmpty(String s) {
        return Objects.isNull(s) || s.equals(EMPTY);
    }

}
