package com.aleflet.task.service;

import com.aleflet.task.modal.database.City;
import com.aleflet.task.modal.dto.CityDto;
import com.aleflet.task.modal.exception.RequestValidationErrorType;
import com.aleflet.task.modal.exception.RequestValidationException;
import com.aleflet.task.repository.CityRepository;
import com.aleflet.task.util.StringUtility;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CityService {

    private final CityRepository cityRepository;

    public CityDto retrieveCities(String name, Pageable pageable) {
        Page<City> result;
        if (StringUtility.isEmpty(name)) {
            result = retrieveCityPage(pageable);
        } else {
            result = searchCities(name, pageable);
        }

        return CityDto.builder()
                .cities(result.stream().toList())
                .totalPages(result.getTotalPages())
                .build();
    }

    private Page<City> retrieveCityPage(Pageable pageable) {
        return cityRepository.findAll(pageable);
    }

    private Page<City> searchCities(String name, Pageable pageable) {
        return cityRepository.findByNameContainingIgnoreCase(name, pageable);
    }

    public void deleteCity(int id) {
        cityRepository.deleteById(id);
    }

    public void updateCity(int id, String name, String image) throws RequestValidationException {
        Optional<City> optionalCity = cityRepository.findById(id);

        if (optionalCity.isEmpty()) {
            throw RequestValidationException.of(RequestValidationErrorType.CITY_NOT_FOUND);
        }

        City city = optionalCity.get();
        city.setName(name);
        city.setImage(image);

        cityRepository.save(city);
    }

}
