package com.aleflet.task.modal.response;

import com.aleflet.task.modal.database.City;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class CityResponse {

    private List<City> cities;

    private int totalPages;

}
