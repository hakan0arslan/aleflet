package com.aleflet.task.modal.request;

import lombok.Data;

@Data
public class UpdateCityRequest {

    private String name;

    private String image;

}
