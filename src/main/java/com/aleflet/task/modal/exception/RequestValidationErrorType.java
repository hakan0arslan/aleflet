package com.aleflet.task.modal.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum RequestValidationErrorType {

    CITY_NOT_FOUND(4000, "City not found for this id.");

    private final int code;

    private final String message;

}
