package com.aleflet.task.modal.exception;

import lombok.Getter;

@Getter
public class RequestValidationException extends Exception {

    private final RequestValidationErrorType errorType;

    private RequestValidationException(RequestValidationErrorType errorType) {
        super(errorType.getMessage());

        this.errorType = errorType;
    }

    public static RequestValidationException of(RequestValidationErrorType type) {
        return new RequestValidationException(type);
    }

}
