package com.aleflet.task.modal.dto;

import com.aleflet.task.modal.database.City;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class CityDto {

    private List<City> cities;

    private int totalPages;

}
