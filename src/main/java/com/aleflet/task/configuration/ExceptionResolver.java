package com.aleflet.task.configuration;

import com.aleflet.task.modal.exception.RequestValidationException;
import com.aleflet.task.modal.response.ErrorResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@RequiredArgsConstructor
public class ExceptionResolver extends ResponseEntityExceptionHandler {

    private static final String UNEXPECTED_ERROR = "Unexpected error happened.";

    @ExceptionHandler(value = {RequestValidationException.class})
    protected ResponseEntity<Object> handleRequestValidationException(RequestValidationException exception, WebRequest request) {
        logger.error(exception);

        ErrorResponse errorResponse = ErrorResponse.builder()
                .message(exception.getErrorType().getMessage())
                .code(exception.getErrorType().getCode())
                .build();

        return handleExceptionInternal(exception, errorResponse, HttpHeaders.EMPTY, HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler()
    protected ResponseEntity<Object> genericHandler(Exception exception, WebRequest request) {
        logger.error(exception);

        ErrorResponse errorResponse = ErrorResponse.builder()
                .message(UNEXPECTED_ERROR)
                .code(HttpStatus.INTERNAL_SERVER_ERROR.value())
                .build();

        return handleExceptionInternal(exception, errorResponse, HttpHeaders.EMPTY, HttpStatus.INTERNAL_SERVER_ERROR, request);
    }

}