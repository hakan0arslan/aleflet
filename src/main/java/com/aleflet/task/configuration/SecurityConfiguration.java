package com.aleflet.task.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration {

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http
                .authorizeHttpRequests()
                .requestMatchers(HttpMethod.PUT, "/api/v*/cities/**")
                .hasRole("ALLOW_EDIT")
                .requestMatchers("/**")
                .permitAll()
                .and()
                .csrf().disable()
                .headers().frameOptions().disable()
                .and().httpBasic();

        return http.build();
    }

    @Bean
    public UserDetailsService userDetailsService() {
        //FIXME This is just to provide login function
        UserDetails user =
                User.withDefaultPasswordEncoder()
                        .username("username")
                        .password("password")
                        .roles("ALLOW_EDIT")
                        .build();

        return new InMemoryUserDetailsManager(user);
    }


}
