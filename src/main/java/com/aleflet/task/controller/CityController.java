package com.aleflet.task.controller;

import com.aleflet.task.modal.dto.CityDto;
import com.aleflet.task.modal.exception.RequestValidationException;
import com.aleflet.task.modal.request.UpdateCityRequest;
import com.aleflet.task.modal.response.CityResponse;
import com.aleflet.task.service.CityService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.converters.models.PageableAsQueryParam;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

@RestController
@Tag(name = "City Services")
@RequestMapping("/api/v1/cities")
@RequiredArgsConstructor
public class CityController {

    private final CityService cityService;

    @GetMapping
    @PageableAsQueryParam
    @Operation(summary = "Returns list of cities")
    public CityResponse retrieveCities(
            @RequestParam(required = false) String name,
            @Parameter(hidden = true)
            @PageableDefault(page = 0, size = 10, sort = "name", direction = Sort.Direction.ASC) Pageable pageable) {
        CityDto cityDto = cityService.retrieveCities(name, pageable);

        return CityResponse.builder().cities(cityDto.getCities()).totalPages(cityDto.getTotalPages()).build();
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Deletes city with id")
    public void deleteCity(@PathVariable int id) {
        cityService.deleteCity(id);
    }

    @PutMapping("/{id}")
    @Operation(summary = "Updates city with id", security = @SecurityRequirement(name = "basicAuth"))
    public void updateCity(@PathVariable int id, @RequestBody UpdateCityRequest request) throws RequestValidationException {
        cityService.updateCity(id, request.getName(), request.getImage());
    }

}
