# Information

Requires Java 17

Maven is used

Service layer tests are written with mock repository

Updating requires basic authentication with username and password

H2 is used to prevent extra database configurations

It will create city table and insert all data when application is started

There is postman collection if required to test

# Links

## For swagger

http://localhost:9797/swagger-ui/index.html

Or deployed here

http://78.47.91.62:9797/swagger-ui/index.html

All endpoints can be tested here

PUT will require basic authentication

Username: username

Password: password

## For Database Ui

http://localhost:9797/h2-console

Driver Class : org.h2.Driver

JDBC URL : jdbc:h2:mem:task

Username : task

Password : password


